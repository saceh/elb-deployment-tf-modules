##########################################
##  Creating AutoScaling Group
###########################################

resource "aws_autoscaling_group" "example" {
  name = "${var.Application_ID}-${var.Environment}-ASG-IAC"
  #availability_zones = ["${var.availability_zones}"]
  vpc_zone_identifier = "${var.vpc_asg_private_subnet}"
  availability_zones = "${data.aws_availability_zones.all.names}"
  #vpc_zone_identifier = "${data.aws_subnet_ids.public_subnet_ids.ids}"
  min_size = "${var.min_size}"
  max_size = "${var.max_size}"
  load_balancers = ["${aws_elb.classic-elb.name}"]
  health_check_type = "ELB"
  health_check_grace_period = 300
  
  launch_template {
    id      = "${aws_launch_template.example.id}"
    version = "$Latest"
  }

  #suspended_processes = ["AddToLoadBalancer", "AlarmNotification", "ScheduledActions"]
  
  tags = "${data.null_data_source.tags.*.outputs}"
}


data "aws_availability_zones" "all" {}
