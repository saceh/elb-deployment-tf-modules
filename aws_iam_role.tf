# create a service role for codedeploy
resource "aws_iam_role" "codedeploy_service" {
  name = "codedeploy-service-role-${var.Environment}-${var.Application_ID}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "codedeploy.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# attach AWS managed policy called AWSCodeDeployRole
# required for deployments which are to an EC2 compute platform
resource "aws_iam_role_policy_attachment" "codedeploy_service" {
  role       = "${aws_iam_role.codedeploy_service.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}

# create a service role for ec2 
resource "aws_iam_role" "instance_profile" {
  name = "codedeploy-instance-profile-${var.Environment}-${var.Application_ID}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# provide ec2 access to s3 bucket to download revision. This role is needed by the CodeDeploy agent on EC2 instances.
resource "aws_iam_role_policy_attachment" "instance_profile_codedeploy" {
  role       = "${aws_iam_role.instance_profile.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforAWSCodeDeploy"
}

resource "aws_iam_role_policy_attachment" "instance_profile_codedeploy_02" {
  role       = "${aws_iam_role.instance_profile.name}"
  policy_arn = "${aws_iam_policy.policy.arn}"
}

resource "aws_iam_instance_profile" "main" {
  name = "codedeploy-instance-profile-${var.Environment}-${var.Application_ID}"
  role = "${aws_iam_role.instance_profile.name}"
}

data "aws_iam_policy_document" "ServiceRolePolicyDocument" {
    statement {
        # Defaults to the current version
        # version = "2012-10-17"
        actions = [
            "s3:*",
            "cloudwatch:*",
            "events:*",
            "codedeploy:*",
            "logs:*",
            "apigateway:*",
            "ses:*",
            "ssm:*",
            "dynamodb:*",
            "athena:*",
            "sns:*",
            "ec2:*",  
        ]
        resources = ["*"]
        effect = "Allow"
    }
}


resource "aws_iam_policy" "policy" {
  name        = "codedeploy-${var.Application_ID}-${var.Environment}-policy"
  description = "A ec2 chatbopt policy"
  policy      = "${data.aws_iam_policy_document.ServiceRolePolicyDocument.json}"
}

resource "aws_iam_policy" "codedeploy_custom" {
  name        = "${var.Project}-CodeDeployCustomPolicy-${var.Application_ID}-${var.Environment}"
  path        = "/"
  description = "IAM:PassRole & EC2:RunInstances"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "iam:PassRole",
        "ec2:CreateTags",
        "ec2:RunInstances"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "codedeploy_service_2_of_2" {
  role       = "${aws_iam_role.codedeploy_service.name}"
  policy_arn = "${aws_iam_policy.codedeploy_custom.arn}"
}