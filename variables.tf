variable "Application_ID" {}
variable "Application_Role" {}
variable "Business_Owner" {}
variable "Environment" {}
variable "Technical_Owner" {}
variable "CrossBUWorkload" {}
variable "Service_Name" {}
variable "Project" {}
variable "OS_Version" {}
variable "Team" {}
variable "Name" {}
variable "Automation_Schedule_Opt_Out" {}
variable "Automation_Schedule_UTC" {}
variable "Business_Unit" {}
variable "Cluster" {}
variable "Compliance" {}
variable "Security" {}
variable "Version" {}
variable "Confidentiality" {}

#########################
## AWS
###########################
variable "app_name" {
  
}

variable "vpc_id_apg" {}
variable "vpc_zone_identifier" {}
#variable "availability_zones" {}
#variable "subnets_alb" {}
variable "vpc_asg_private_subnet" {}

variable "packer_ami_name" {}
variable "s3_bucket_name" {}
variable "instance_type" {}
variable "key_name" {}
variable "min_size" {}
variable "max_size" {}
variable "health_check_target" {}
#variable "certificate_arn" {}


variable "vpc_cidr" {}


variable "volume_size" {}
variable "volume_type" {}
#variable "placement_tenancy" {}

variable "scale_up_threshold" {}
variable "scale_down_threshold" {}
variable "metric_name" {}
variable "listener_instance_port" {}

variable "ingress_to_port_01" {}
variable "ingress_to_port_02" {}
variable "ingress_from_port_02" {}
variable "ingress_from_port_01" {}

#variable "trigger_target_arn" {}
