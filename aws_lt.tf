#######################
# Launch Template
#######################

resource "aws_launch_template" "example" {
  name = "${var.Application_ID}-${var.Environment}-lt-iac"

# Launch template contents
  image_id      = "${data.aws_ami.ec2-ami.id}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  # vpc_security_group_ids = ["${aws_security_group.instance.id}"]

# Network interfaces
  network_interfaces {
    associate_public_ip_address = true
    delete_on_termination = false
    # subnet_id  = "${data.aws_subnet.dec_vpc_subnet_1A.*.id[1]}"
    security_groups = ["${aws_security_group.http.id}"] 

  }

# Storage (Volumes)
  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      volume_size = "${var.volume_size}" // e.g. 100
      volume_type = "${var.volume_type}" // e..g gp2
      encrypted   = true
    #   delete_on_termination = true
    #   kms_key_id  = "arn:aws:kms:ap-southeast-1:615302655748:key/e71cbac7-7dd0-4136-944d-363660c9dfc0"
      
    }

  }

# Advanced details
  iam_instance_profile {
    arn = "${aws_iam_instance_profile.main.arn}"
  }
  
  instance_initiated_shutdown_behavior = "stop"
  disable_api_termination = false
  
  monitoring {
    enabled = true
  }
  
  ebs_optimized = false
  
  # disable T2/T3 Unlimited
  credit_specification {
    cpu_credits = "standard"
  }
  
  #placement {
  #  tenancy = "${var.placement_tenancy}" // e.g. default, dedicated or host
  #}

  user_data = "${base64encode(data.template_file.user_data_hw.rendered)}"

  tag_specifications {
    resource_type = "instance"
    tags = "${merge(
      local.common_tags,
      map("Name", "${var.Application_ID}-${var.Environment}-ec2-iac")
    )}"
  }

}

data "template_file" "user_data_hw" {
  template = <<EOF
#!/bin/bash
echo "Hello World"
EOF
}