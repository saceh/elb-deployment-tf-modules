###################
### Creating Classic LoadBalancer
####################

resource "aws_elb" "classic-elb" {
  name               = "${var.Application_ID}-${var.Environment}-elb-tf"
  # availability_zones = ["ap-southeast-1", "ap-southeast-1b"]

  #access_logs {
  #  bucket        = "${aws_s3_bucket.bucket.id}"
  #  bucket_prefix = "logs"
  #  interval      = 5         # in minutes
  #
  #}

  health_check {
    healthy_threshold   = "3"
    unhealthy_threshold = "5"
    interval            = "30"
    timeout             = "5"
    target              = "${var.health_check_target}"

  }

  listener {
    instance_port       = "${var.listener_instance_port}"
    instance_protocol   = "http"
    lb_port             = 80
    lb_protocol         = "http"
    #ssl_certificate_id  = "${var.certificate_arn}"

  }

  listener {
    instance_port       = "5000"
    instance_protocol   = "http"
    lb_port             = 5000
    lb_protocol         = "http"
    #ssl_certificate_id  = "${var.certificate_arn}"

  }

  security_groups = ["${aws_security_group.elb.id}"]
  subnets         = "${var.vpc_zone_identifier}"

  tags = {
    Name          = "${var.Application_ID}-${var.Environment}-${var.Compliance}"
    Environment   = "${var.Environment}"
    Version       = "${var.Version}"
  }

}

########################
data "aws_elb_service_account" "main" {}

data "aws_iam_policy_document" "s3_lb_write" {
  policy_id = "s3_lb_write"
  statement {
      actions = ["s3:PutObject"]
      resources = ["arn:aws:s3:::${var.s3_bucket_name}/logs/*"]

      principals {
          identifiers = ["${data.aws_elb_service_account.main.arn}"]
          type = "AWS"
      }
  }
}

# Create a new ALB Target Group attachment
resource "aws_autoscaling_attachment" "asg_attachment_bar" {
  autoscaling_group_name = "${aws_autoscaling_group.example.id}"
  elb                    = "${aws_elb.classic-elb.id}"
}
