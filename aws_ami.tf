############
# GET LATEST AMI
############
data "aws_ami" "ec2-ami" {
  filter {
    name   = "state"
    values = ["available"]
  }
  filter {
    name   = "tag:Name"
    values = ["${var.packer_ami_name}"]
  }
  most_recent = true
  owners   = ["self"]
}
