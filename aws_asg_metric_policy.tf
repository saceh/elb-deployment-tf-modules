# scale up alarm
resource "aws_autoscaling_policy" "example-cpu-policy-scaleup" {
    name = "${var.Application_ID}-${var.Environment}-cpu-policy-scaleup"
    autoscaling_group_name = "${aws_autoscaling_group.example.name}"
    adjustment_type = "ChangeInCapacity"
    scaling_adjustment = "1"
    cooldown = "300"
    policy_type = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "example-cpu-alarm-scaleup" {
    alarm_name = "${var.Application_ID}-${var.Environment}-cpu-alarm-scaleup"
    alarm_description = "Greater (>=) Threshold Based on CPUUtilization (Scale Up)"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "${var.metric_name}"
    namespace = "AWS/EC2"
    period = "120"
    statistic = "Average"
    threshold = "${var.scale_up_threshold}"
    dimensions = {
        "AutoScalingGroupName" = "${aws_autoscaling_group.example.name}"
    }
    actions_enabled = true
    alarm_actions = ["${aws_autoscaling_policy.example-cpu-policy-scaleup.arn}"]
}

# scale down alarm
resource "aws_autoscaling_policy" "example-cpu-policy-scaledown" {
    name = "${var.Application_ID}-${var.Environment}-cpu-policy-scaledown"
    autoscaling_group_name = "${aws_autoscaling_group.example.name}"
    adjustment_type = "ChangeInCapacity"
    scaling_adjustment = "-1"
    cooldown = "300"
    policy_type = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "example-cpu-alarm-scaledown" {
    alarm_name = "${var.Application_ID}-${var.Environment}-cpu-alarm-scaledown"
    alarm_description = "Lesser (<=) Threshold Based on CPUUtilization (Scale Down)"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "${var.metric_name}"
    namespace = "AWS/EC2"
    period = "120"
    statistic = "Average"
    threshold = "${var.scale_down_threshold}"
    dimensions = {
        "AutoScalingGroupName" = "${aws_autoscaling_group.example.name}"
    }
    actions_enabled = true
    alarm_actions = ["${aws_autoscaling_policy.example-cpu-policy-scaledown.arn}"]
}