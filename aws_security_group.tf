############################################
# SECURITY GROUP FOR EC2
############################################
resource "aws_security_group" "http" {
  name        = "${var.Application_ID}-${var.Environment}-ec2-sg"
  description = "Set security group for EC2 Instances"
  #vpc_id      = "${data.aws_vpc.vpc_id.id}"
  vpc_id = "${var.vpc_id_apg}"
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  lifecycle {
        create_before_destroy = true
    }
}

resource "aws_security_group_rule" "tcp8080" {
  
  type              = "ingress"
  from_port         = "${var.ingress_from_port_01}"
  to_port           = "${var.ingress_to_port_01}"
  protocol          = "tcp"
  cidr_blocks       = ["${var.vpc_cidr}"]
  description       = "elb sec grp"
  security_group_id = "${aws_security_group.http.id}"
  #self = "true"
}

resource "aws_security_group_rule" "tcp" {
  
  type              = "ingress"
  from_port         = "${var.ingress_from_port_02}"
  to_port           = "${var.ingress_to_port_02}"
  protocol          = "tcp"
  cidr_blocks       = ["${var.vpc_cidr}"]
  description       = "elb sec grp"
  security_group_id = "${aws_security_group.http.id}"
  #self = "true"
}

resource "aws_security_group_rule" "tcpingrgess" {
  
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  cidr_blocks       = ["${chomp(data.http.myip.body)}/32"]
  description       = "elb sec grp"
  security_group_id = "${aws_security_group.http.id}"
}

############################################
## Security Group for ELB
############################################
resource "aws_security_group" "elb" {
  name = "${var.Application_ID}-${var.Environment}-elb-sg"
  #vpc_id = "${data.aws_vpc.vpc_id.id}"
  vpc_id = "${var.vpc_id_apg}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 5000
    to_port = 5000
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
        create_before_destroy = true
    }
}

output "internal_security_group_id" {
  value = "${aws_security_group.http.id}"
}


#######################
##  WHAT IS MY IP
#####################
data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}
