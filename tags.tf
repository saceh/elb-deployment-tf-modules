########
# TAGS
#######
locals {
  common_tags = {
    Application_ID   =  "${var.Application_ID}"
    Application_Role =  "${var.Application_Role}"
    Business_Owner   =  "${var.Business_Owner}"
    Environment      =  "${var.Environment}"
    Technical_Owner  =  "${var.Technical_Owner}"
    CrossBUWorkload  =  "${var.CrossBUWorkload}"
    Service_Name     =  "${var.Service_Name}"
    Project          =  "${var.Project}"
    OS_Version       =  "${var.OS_Version}"
    Team             =  "${var.Team}"
    Name             =  "${var.Name}"
    Automation_Schedule_Opt_Out = "${var.Automation_Schedule_Opt_Out}"
    Automation_Schedule_UTC  = "${var.Automation_Schedule_UTC}"
    Business_Unit  = "${var.Business_Unit}"
    Cluster  = "${var.Cluster}"
    Compliance = "${var.Compliance}"
    Security  = "${var.Security}"
    version  = "${var.Version}"
    Confidentiality = "${var.Confidentiality}"
  }
}

data "null_data_source" "tags" {
  count = "${length(keys(local.common_tags))}"

  inputs = {
    key                 = "${element(keys(local.common_tags), count.index)}"
    value               = "${element(values(local.common_tags), count.index)}"
    propagate_at_launch = true
  }
}


