
#############################
# create a deployment group
#############################
resource "aws_codedeploy_deployment_group" "main" {
  app_name              = "${var.app_name}"
  deployment_group_name = "${var.Application_ID}-${var.Environment}-grp-iac"
  autoscaling_groups    = ["${aws_autoscaling_group.example.id}"]
  service_role_arn      = "${aws_iam_role.codedeploy_service.arn}"

  deployment_config_name = "CodeDeployDefault.OneAtATime" # AWS defined deployment config
  
  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }

  load_balancer_info {
    elb_info {
      name = "${aws_elb.classic-elb.name}"
    }
  }
  
  # trigger a rollback on deployment failure event
  auto_rollback_configuration {
    enabled = true
    events = [
      "DEPLOYMENT_FAILURE",
    ]
  }
  blue_green_deployment_config {
    deployment_ready_option {
      action_on_timeout    = "CONTINUE_DEPLOYMENT"
    }

    green_fleet_provisioning_option {
      action = "COPY_AUTO_SCALING_GROUP"
    }

    terminate_blue_instances_on_deployment_success {
      action = "TERMINATE"
    }
  }
  #trigger_configuration {
  #  trigger_events      = ["DeploymentStart", "DeploymentSuccess", "DeploymentFailure", "DeploymentStop", "DeploymentRollback"]
  #  trigger_name        = "${var.Project}-${var.Application_ID}-${var.Environment}-Deployment_Status"   // name of notification trigger
    #trigger_target_arn  = "${var.trigger_target_arn}"
  #}
}