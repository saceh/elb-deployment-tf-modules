# Frequently Asked Questions (FAQs)

1. Do we need additional 'vars' for this to work?  
⋅⋅⋅ANSWER: Yes

2. How will having additional 'vars' benefit us?  
⋅⋅⋅ANSWER: Finer-grained controls (such as shutdown behaviour, EBS_optimized, encryption, etc.)

3. How will having additional 'vars' challenge us?  
⋅⋅⋅ANSWER: Older resources that uses this modules will no longer be able to interact with it! **(CAUTION)**

4. What are the new variables to be added?  
⋅⋅⋅ANSWER: var.volume_size, var.volume_type, var.placement_tenancy

5. What are the benefits / differences / challeges?  
⋅⋅⋅ANSWER: [Link Here!: EC2 Launch Templates](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-launch-templates.html)

6. Files different to aws-deployment-tf-modules (As of 18SEP2019)  
⋅⋅⋅ANSWER: aws_asg.tf, aws_lt.tf

7. Variables to pass on?  
⋅⋅⋅ANSWER: (BELOW)



<details><summary>Cool Dropdown #1</summary><br>

TAGS  
======  
Application_ID          =  "ABD"  
Application_Role        =  "apg-biz-dashboard-iac"  
Business_Owner          =  "gautam_jain@astro.com.my"  
Environment             =  "DEV"  
Technical_Owner         =  "nicholas_ngoo@astro.com.my"  
CrossBUWorkload         =  "NO"  
Service_Name            =  "PCIDSS-API-Dev"  
Project                 =  "APG"  
OS_Version              =  "Linux Centos"  
Team                    =  "APG"  
Name                    =  "apg-biz-dashboard-iac" # instance name  
Automation_Schedule_Opt_Out = "NO"  
Automation_Schedule_UTC     = "UPTIME-MTWRFXX-0100-1500"  
Business_Unit           = "APG"  
Cluster                 = "NO"  
Compliance              = "PCI"  
Security                = "PCI"  
Confidentiality         = "NO"  
build_version           = "001"  

</details>


<details><summary>Cool Dropdown #1</summary><br>

APP  
======  
domain                  = ""  
acm_type                = ""  
// packer_ami_name       = ""  
s3_bucket_name          = ""  
instance_type           = ""  
key_name                = ""  
min_size                = ""  
max_size                = ""  
vpc_name                = ""  
public_subnet_name      = ""  
private_subnet_name     = ""  
vpc_cidr                = ""  
+volume_size            = ""  
+volume_type            = ""  
+placment_tenancy       = ""  

</details>
